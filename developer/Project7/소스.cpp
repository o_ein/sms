#include "Turboc.h"
#include <mmsystem.h>
#include <fstream>
#include <conio.h>
#include <wchar.h>
#include <stdlib.h>
#include "def.h"

#pragma comment(lib, "winmm.lib")

int SMGK();
int main();

//텍스트의 색을 바꾸기
void textcolor(int foreground, int background)
{
	int color = foreground + background * 16;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color);
}

//마우스 커서 클릭 버튼
int showClickPositionInConsole() // 0 왼쪽클릭 1 가운데 클릭 2 오른쪽 클릭
{
	INPUT_RECORD input_record;
	DWORD input_count;
	HANDLE hIN = GetStdHandle(STD_INPUT_HANDLE);

	SetConsoleMode(hIN, ENABLE_EXTENDED_FLAGS | ENABLE_WINDOW_INPUT | ENABLE_MOUSE_INPUT);
		ReadConsoleInput(hIN, &input_record, 1, &input_count);

		if (input_record.EventType == MOUSE_EVENT)
		{
			if (input_record.Event.MouseEvent.dwButtonState & FROM_LEFT_1ST_BUTTON_PRESSED)
			{
				return 0;
			}else if (input_record.Event.MouseEvent.dwButtonState & FROM_LEFT_2ND_BUTTON_PRESSED)
			{
				return 1;
			}
			else if (input_record.Event.MouseEvent.dwButtonState & RIGHTMOST_BUTTON_PRESSED) {
				return 2;
			}
		}
		else if (input_record.EventType == KEY_EVENT)
		{
			if (input_record.Event.KeyEvent.bKeyDown)
			{

			}
		}
	return -1;
}

//-   -출력
void md(int i) {
	for (int j = 1; j < i; j++) {
		printf("-");
		for (int i = 0; i < 78; i++) printf(" ");
		printf("-");
	}
}

//잡는 방법
int HowToGrab(int mod) {
	system(" mode con lines=26 cols=80 ");
	textcolor(LIGHTGRAY, BLACK);
	PlaySound(TEXT(Start), NULL, SND_FILENAME | SND_ASYNC);
	gotoxy(0, 0);
	for (int i = 0; i < 80; i++) printf("-");
	printf("-");
	for (int i = 0; i < 78; i++) printf(" ");
	printf("-");
	md(2+9);
	printf("-");
	for (int i = 0; i < 78; i++) {
		if (i == 20 && mod == 1) {
			printf("이 게임은 가로 잡기로 플레이 합니다.                      ");
			break;
		}
		else if (i == 20 && mod == 0) {
			printf("이 게임은 세로 잡기로 플레이 합니다.                      ");
			break;
		}
		else if (i == 16 && mod == 2) {
			printf("이 게임은 세로 잡기 또는 가로잡기로 플레이 합니다.            ");
			break;
		}
		else {
			printf(" ");
		}
	}
	printf("-");
	md(21-9);
	for (int i = 0; i < 80; i++) printf("-");
	return 1;
}

//타이틀 출력
int Title(int mod) {
	system(" mode con lines=26 cols=80 ");
	if(mod == 3) textcolor(LIGHTGRAY, BLACK);
	else textcolor(CYAN, BLACK);
	gotoxy(0, 0);
	for (int i = 0; i < 80; i++) printf("-");
	md(11);
	if (mod == 0) {
		PlaySound(TEXT(Theme), NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
		printf("-                           SUPER MELYEOWO SERIES.                             -");
		textcolor(YELLOW, BLACK);
		printf("-                            슈퍼 메려워 시리즈.                               -");
	}else if (mod == 1){
		PlaySound(TEXT(Kart), NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
		printf("-                           SUPER MELYEOWO KART.                               -");
		textcolor(YELLOW, BLACK);
		printf("-                            슈퍼 메려워 카트.                                 -");
	}else if (mod == 2) {
		PlaySound(TEXT(Kart), NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
		printf("-                        SUPER MELYEOWOUN GEOJI KIUGI.                         -");
		textcolor(YELLOW, BLACK);
		printf("-                          슈퍼 메려운 거지 키우기.                            -");
	}else if (mod == 3){
		md(3);
		md(21 - 9);
		for (int i = 0; i < 80; i++) printf("-");
		return 1;
	}
	
	md(21 - 9);
	for (int i = 0; i < 80; i++) printf("-");
	gotoxy(33 - 2, 21);
	printf("Press START or B");
	while (showClickPositionInConsole() < 1 || showClickPositionInConsole() > 1) Sleep(1);
	PlaySound(TEXT(Select), NULL, SND_FILENAME);
	return 1;
}

//로딩
void titleing(int ean) {
	system(" mode con lines=26 cols=80 ");
	PlaySound(TEXT(SelectMenu), NULL, SND_FILENAME | SND_ASYNC);
	gotoxy(35, 11);
	printf("Load Game");
	for (int i = 0; i <= ean; i++) {
		system("title Load Game.");
		Sleep(100);
		system("title Load Game..");
		Sleep(100);
		system("title Load Game...");
		Sleep(100);
		system("title Load Game....");
		Sleep(100);
		system("title Load Game.....");
		Sleep(100);
	}
}

//SMK 인게임
int SMKI() {
	system(" mode con lines=26 cols=80 ");
	gotoxy(1, 25);
	return 0;
}

//슈퍼 매려워 카트
int SMK() {
	system(" mode con lines=26 cols=80 ");
	system("cls");
	titleing(4);
	system("title SUPER MELYEOWO Kart. By 2020 SYINTENDO");
	system("cls");
	HowToGrab(0);
	Sleep(1500);
	int a = Title(1);
	a = SMKI();
	return 0;
}

//SMGK 캐릭
int SMGKCP() {
	gotoxy(50, 17);
	printf("○");
	gotoxy(49, 18);
	printf("─╋─");
	gotoxy(50, 19);
	printf("│");
	gotoxy(49, 20);
	printf("┌┴┐");
	return 1;
}

void SMGBP(int y) {
	gotoxy(5, y);
	printf("♨");
}

int SMGKM(bool mod) {
	int x = SMGKCP();
	gotoxy(19, 5);
	if (mod)printf("윽! 똥이 마려운데 변기가 없어!");
	else printf("하지만 변기를 살돈도 없어.");
	while (_kbhit() == false && showClickPositionInConsole() == -1)Sleep(1);
	return 111;
}

//SMGK엔딩
void SMGKE() {
	Title(3);
	gotoxy(27, 10);
	printf("Thank you for Playing!!!");
	gotoxy(15, 11);
	printf("이 게임을 다시시작하려면 A 끝내려면 B를 눌러주세요.");
	Sleep(100);
	int ax = showClickPositionInConsole();
	while (ax == -1 && ax == 1) Sleep(0);
	if (ax == 0) SMGK();
	else main();
}

//슈퍼 마려운 거지 키우기
int SMGK(){
	

	system(" mode con lines=26 cols=80 ");
	system("cls");
	titleing(4);
	system("title SUPER MELYEOWOUN GEOJI KIUGI. By 2020 SYINTENDO");
	system("cls");
	HowToGrab(2);
	Sleep(1500);
	int a = Title(2);
	system("cls");
	textcolor(LIGHTGRAY, BLACK);
	a = Title(3);
	a = SMGKM(true);
	system("cls");
	a = Title(3);
	a = SMGKM(false);
	system("cls");
	a = Title(3);
	a = SMGKCP();
	gotoxy(10, 3);

	int money = 0;
	int level = 0;
	printf("%d원이 있음   %d레벨   ", money , level + 1);
	gotoxy(10, 5);
	printf("레벨업 비용은 시세와 레벨의 따라 변동될수 있습니다.");
	int axe = (rand() % 2 + 1);
	while (true)
	{
		gotoxy(10, 3);
		printf("%d원이 있음   %d레벨   ", money, level + 1);
		int gx = showClickPositionInConsole();
		if (gx != -1 && gx != 1) {
			money = money + (level + 1);
			gotoxy(10, 3);
			printf("%d원이 있음   %d레벨   ", money, level + 1);
			PlaySound(TEXT(Coin), NULL, SND_FILENAME | SND_ASYNC);
			Sleep(25);
		}
		if (money >= (level + 1) * 100 + axe) {
			money = money - (level + 1) * 100 + axe;
			level = level + 1;
			axe = (rand() % 2 + 1);
			PlaySound(TEXT(Up), NULL, SND_FILENAME | SND_ASYNC);
		}
		if (level + 1 >= 300) {
			system("cls");
			SMGKE();
			return 999;
		}else if (level+1 >= 100) {
			SMGBP(3);
		}else if (level+1 >= 95) {
			SMGBP(4);
		}
		else if (level+1 >= 80) {
			SMGBP(5);
		}
		else if (level+1 >= 75) {
			SMGBP(6);
		}
		else if (level+1 >= 70) {
			SMGBP(7);
		}
		else if (level+1 >= 65) {
			SMGBP(8);
		}
		else if (level+1 >= 60) {
			SMGBP(9);
		}
		else if (level+1 >= 55) {
			SMGBP(10);
		}
		else if (level+1 >= 50) {
			SMGBP(11);
		}
		else if (level+1 >= 45) {
			SMGBP(12);
		}
		else if (level+1 >= 40) {
			SMGBP(13);
		}
		else if (level+1 >= 35) {
			SMGBP(14);
		}
		else if (level+1 >= 30) {
			SMGBP(15);
		}
		else if (level+1 >= 25) {
			SMGBP(16);
		}
		else if (level+1 >= 20) {
			SMGBP(17);
		}
		else if (level+1 >= 15) {
			SMGBP(18);
		}
		else if (level+1 >= 10) {
			SMGBP(19);
		}
		else if (level+1 >= 5) {
			SMGBP(20);
		}
		gotoxy(10, 4);
		printf("레벨업을 하려면 %d원이 필요합니다.", ((level + 1) * 100) + axe);
	}
	return 0;
}



//게임 선택 화면
int GST() {
	system(" mode con lines=26 cols=80 ");
	PlaySound(TEXT(SelectMenu), NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
	textcolor(CYAN, BLACK);
	gotoxy(0, 0);
	for (int i = 0; i < 80; i++) printf("-");
	md(12);
	textcolor(YELLOW, BLACK);
	md(21 - 8);
	for (int i = 0; i < 80; i++) printf("-");
	gotoxy(5, 3);
	printf("A     . 슈퍼 마려운 거지 키우기");
	gotoxy(5, 4);
	printf("START . 추후 업데이트 에정");
	gotoxy(5, 5);
	printf("B     . 추후 업데이트 에정");
	int sel = -1;
	while (true)
	{
		if (showClickPositionInConsole() == 0) {
			system("cls");
			sel = 0;
			break;
		}
		else if (showClickPositionInConsole() == 1) {
			sel = 1;
			break;
		}
		else if (showClickPositionInConsole() == 2) {
			sel = 2;
			break;
		}
	}
	if (sel == 0) int a = SMGK();
	return 1;
}

//메인
int main()
{
	HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO ConsoleCursor;
	ConsoleCursor.bVisible = 0;
	ConsoleCursor.dwSize = 1;
	SetConsoleCursorInfo(consoleHandle, &ConsoleCursor);
	system(" mode con lines=26 cols=80 ");
	titleing(6);
	system("cls");
	system("title SUPER MELYEOWO SERIES. By 2020 SYINTENDO");
	int a = HowToGrab(2);
	Sleep(1500);
	system("cls");
	a = Title(0);
	a = GST();
}
